# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)



--------------------------------

## v0.2.0, 2024.09.10

- Use Hash data type parameter for all webapp configs #8


--------------------------------

## v0.1.0, 2024.06.14

Initial release.


--------------------------------

## Template

```markdown
## <major>.<minor>.patch_DEV     (unreleased)

### Added

### Changed

### Fixed

### Security

--------------------------------

```
