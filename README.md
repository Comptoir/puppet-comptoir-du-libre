# Comptoir

## Table of Contents

1. [Description](#description)
1. [Setup - The basics of getting started with comptoir](#setup)
    * [What Comptoir-du-Libre affects](#what-comptoir-du-libre-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with Comptoir-du-Libre](#beginning-with-comptoir-du-libre)
1. [Usage - Configuration options and additional functionality](#usage)
1. [Limitations - OS compatibility, etc.](#limitations)
1. [Development - Guide for contributing to the module](#development)

## Description

Deploy and configure [Comptoir-du-Libre](https://gitlab.adullact.net/Comptoir/comptoir-du-libre).

## Setup

### What Comptoir-du-Libre affects

This module install and configure Comptoir-du-Libre.

By default, a PostgreSQL server is installed and configured locally, a role and a database are created.

### Setup Requirements

As a web app, you have to configure a web server with php configured. To do so with Puppet, it is possible to use :
 * [puppetlabs-apache](https://forge.puppet.com/modules/puppetlabs/apache/readme)
 * [puppet-nginx](https://forge.puppet.com/modules/puppet/nginx/readme)
 * [puppet-php](https://forge.puppet.com/modules/puppet/php/readme)

As backend, only PostgreSQL is supported. With Puppet, it is possible to use [puppetlabs-postgresql](https://forge.puppet.com/modules/puppetlabs/postgresql/readme).

Comptoir-du-Libre send some mail to webapp users.
You have to ensure to set SMTP attributes accordingly with your SMTP infrastructure.
With Puppet, it is possible to use [puppet-postfix](https://forge.puppet.com/modules/puppet/postfix/readme).

### Beginning with Comptoir-du-Libre

The very basic steps can be simple `include comptoir`.

## Usage

If you are using Apache, this setup is possible :

```ruby
class { 'comptoir':
  app_secret        => 'ThisIsMySecetUsedToGenerateCSRFTokens',
  smtp_host         => 'smtp.example.org',
  smtp_port         => 465,
  smtp_user         => 'comptoir-username%40example.org',
  smtp_password     => 'comptoir-smtppassword',
  sys_rootpath      => '/var/www/comptoir.example.org',
  sys_rootpath_mode => '0700',
  sys_user          => 'www-data',
  sys_group         => 'www-data',
  webapp            => {
    trusted_hosts => '^comptoir.example.org$',
  },
}
```

## Reference

Details are in [REFERENCE.md](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/-/blob/main/REFERENCE.md) file.

## Limitations

Supported OSes are given in [metadata.json](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/-/blob/main/metadata.json) file.

## Development

Home at URL https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre

Issues and MR are welcome.

## Release Notes/Contributors/Etc.

Details in [CHANGELOG](https://gitlab.adullact.net/Comptoir/puppet-comptoir-du-libre/-/blob/main/CHANGELOG.md).

```
Copyright (C) 2024 Association des Développeurs et Utilisateurs de Logiciels Libres
                     pour les Administrations et Colléctivités Territoriales.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/agpl.html>.

```

