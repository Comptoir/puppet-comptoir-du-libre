# frozen_string_literal: true

require 'spec_helper'

describe 'comptoir' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(service_provider: 'systemd') }

      it { is_expected.to compile }
      it { is_expected.to contain_archive('comptoir.tgz') }

      context 'with defaults' do
        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^APP_ENV=prod$}))

          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^APP_SECRET=ThisTokenIsNotSoSecretChangeIt$}))
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^MAILER_DSN=smtp://127.0.0.1:25$}))
        end
      end

      context 'with custom database settings' do
        let(:params) do
          {
            db_host: 'db.example.org',
            db_version: '16',
            db_user: 'database_user',
            db_password: 'database_password',
            db_name: 'database_name',
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(%r{^DATABASE_URL="postgresql://database_user:database_password@db.example.org:5432/database_name\?serverVersion=16&charset=utf8"$})
        end
      end

      context 'with custom app_secret' do
        let(:params) do
          {
            app_secret: 'ThisCustomTokenIsNotSoSecret',
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^APP_SECRET=ThisCustomTokenIsNotSoSecret$}))
        end
      end

      context '/etc/comptoir/env.prod.local with custom SMTP host and without SMTP auth' do
        let(:params) do
          {
            smtp_host: 'smtp.example.com',
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^MAILER_DSN=smtp://smtp.example.com:25$}))
        end
      end

      context 'with SMTP auth and custom SMTP host and port' do
        let(:params) do
          {
            smtp_host: 'smtp.example.org',
            smtp_port: 465,
            smtp_user: 'mysmtpuser',
            smtp_password: 'mysmtppwd',
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^MAILER_DSN=smtp://mysmtpuser:mysmtppwd@smtp.example.org:465$}))
        end
      end

      context 'with custom WEBAPP_DEFAULT_URI' do
        let(:params) do
          {
            webapp: {
              default_uri: 'http://new-comptoir.example.org/',
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_DEFAULT_URI='http://new-comptoir.example.org/'$}))
        end
      end

      context 'with custom emails (mail from, mail alerting to)' do
        let(:params) do
          {
            webapp: {
              email_from: 'custormer-contact@example.org',
              email_alerting_to: 'webapp-alerting@example.org',
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_EMAIL_FROM='custormer-contact@example.org'$}))
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_EMAIL_ALERTING_TO='webapp-alerting@example.org'$}))
        end
      end

      context 'with custom webapp name' do
        let(:params) do
          {
            webapp: {
              name: 'The New Comptoir',
              shortname: 'New-Comptoir',
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_NAME='The New Comptoir'$}))
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_SHORTNAME='New-Comptoir'$}))
        end
      end

      context 'with locale fr' do
        let(:params) do
          {
            webapp: {
              i18n_default_locale: 'fr',
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_I18N_DEFAULT_LOCALE='fr'}))
        end
      end

      context 'with custom session lifetime' do
        let(:params) do
          {
            webapp: {
              session_lifetime: 60 * 60 * 24 * 30, # 60 seconds * 60 minutes * 24 hours * 30 days = 2 592 000 seconds
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_SESSION_LIFETIME=2592000$}))
        end
      end

      context 'with webapp version displayed publicly (true)' do
        let(:params) do
          {
            webapp: {
              software_version_displayed_publicly: true,
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_SOFTWARE_VERSION_DISPLAYED_PUBLICLY=true$}))
        end
      end

      context 'with webapp version displayed publicly (false)' do
        let(:params) do
          {
            webapp: {
              software_version_displayed_publicly: false,
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_SOFTWARE_VERSION_DISPLAYED_PUBLICLY=false$}))
        end
      end

      context 'with custom timezone: Pacific/Tahiti' do
        let(:params) do
          {
            webapp: {
              timezone: 'Pacific/Tahiti',
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_TIMEZONE='Pacific/Tahiti'$}))
        end
      end

      context '/etc/comptoir/env.prod.local with custom trusted_hosts' do
        let(:params) do
          {
            webapp: {
              trusted_hosts: '^example.org$',
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_TRUSTED_HOSTS='\^example\.org\$'$}))
        end
      end

      context 'with custom min password length' do
        let(:params) do
          {
            webapp: {
              user_config_min_password_length: 16,
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_USER_CONFIG_MIN_PASSWORD_LENGTH=16$}))
        end
      end

      context 'with custom password reset token lifetime' do
        let(:params) do
          {
            webapp: {
              user_config_password_reset_token_lifetime: 600, # 600 seconds
            }
          }
        end

        it do
          is_expected.to contain_file('/etc/comptoir/env.prod.local') \
            .with_content(sensitive(%r{^WEBAPP_USER_CONFIG_PASSWORD_RESET_TOKEN_LIFETIME=600$}))
        end
      end
    end
  end
end
