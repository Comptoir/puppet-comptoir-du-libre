require 'spec_helper_acceptance'

sys_rootpath = '/var/www/comptoir'
##################################################################################################################
version_intitial = '3.0.0.alpha.7'
download_checksum_initial = '2d9099a51b3cd43633e7784e50f734aaf76206660c8c03f5c5f9549899ce22c9'
download_url_initial = 'https://gitlab.adullact.net/Comptoir/comptoir-du-libre/-/package_files/1059/download'
##################################################################################################################
version_upgrade = '3.0.0.alpha.8'
download_checksum_upgrade = 'aafabaaf7ca309569d087116d6067e9ccbf8ad3a699bd28a2c5dd451430cfdf0'
download_url_upgrade = 'https://gitlab.adullact.net/Comptoir/comptoir-du-libre/-/package_files/1060/download'
##################################################################################################################
config_path = '/etc/comptoir'
archive_rootdir = 'comptoir'
var_path = '/var/comptoir'

describe 'comptoir' do
  context 'with basic settings' do
    pp = %(
      class { 'comptoir':
        download_url      => '#{download_url_initial}',
        download_checksum => '#{download_checksum_initial}',
        sys_rootpath      => '#{sys_rootpath}',
        sys_rootpath_mode => '0700',
        sys_user          => 'www-data',
        sys_group         => 'www-data',
        webapp            => {
          software_version_displayed_publicly => true,
          trusted_hosts                       => '127.0.0.1',
        },
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe file("#{sys_rootpath}/#{archive_rootdir}") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}" }
    end

    describe command('curl -i http://127.0.0.1') do
      its(:stdout) { is_expected.to match %r{HTTP/1.1 308 Permanent Redirect} }
      its(:stdout) { is_expected.to match %r{Location: /fr/} }
      its(:stdout) { is_expected.to match %r{x-comptoir-webapp: 1} }
    end

    describe command('curl -i http://127.0.0.1/fr/') do
      its(:stdout) { is_expected.to match %r{HTTP/1.1 200 OK} }
      its(:stdout) { is_expected.to match %r{x-comptoir-webapp: 1} }
    end

    describe command('curl --head http://127.0.0.1/health-check') do
      its(:stdout) { is_expected.to match %r{HTTP/1.1 200 OK} }
      its(:stdout) { is_expected.to match %r{x-comptoir-database-status: DB_CONNECTION_SUCCESSFUL} }
      its(:stdout) { is_expected.to match %r{x-comptoir-version: #{version_intitial}} }
    end

    describe file(config_path.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{config_path}/env.prod.local") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 600 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
      its(:content) { is_expected.to contain "WEBAPP_TRUSTED_HOSTS='127.0.0.1'" }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/.env.local") do
      it { is_expected.to be_file }
      it { is_expected.to be_mode 600 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
      its(:content) { is_expected.to contain 'APP_ENV=prod' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/.env.prod.local") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{config_path}/env.prod.local" }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var") do
      it { is_expected.not_to be_symlink }
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/cache") do
      it { is_expected.not_to be_symlink }
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/cache/prod") do
      it { is_expected.to be_directory }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/log") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{var_path}/log" }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/var/sessions") do
      it { is_expected.to be_symlink }
      it { is_expected.to be_linked_to "#{var_path}/sessions" }
    end

    describe file(var_path.to_s) do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{var_path}/cache") do
      it { is_expected.not_to be_directory }
    end

    describe file("#{var_path}/log") do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end

    describe file("#{var_path}/sessions") do
      it { is_expected.to be_directory }
      it { is_expected.to be_mode 700 }
      it { is_expected.to be_owned_by 'www-data' }
      it { is_expected.to be_grouped_into 'www-data' }
    end
  end

  context 'with upgrade' do
    pp = %(
      class { 'comptoir':
        download_url      => '#{download_url_upgrade}',
        download_checksum => '#{download_checksum_upgrade}',
        sys_rootpath      => '#{sys_rootpath}',
        sys_rootpath_mode => '0700',
        sys_user          => 'www-data',
        sys_group         => 'www-data',
        webapp            => {
          software_version_displayed_publicly => true,
          trusted_hosts                       => '127.0.0.1',
        },
      }
    )

    it 'applies without error' do
      apply_manifest(pp, catch_failures: true)
    end
    it 'applies idempotently' do
      apply_manifest(pp, catch_changes: true)
    end

    describe command('curl -i http://127.0.0.1') do
      its(:stdout) { is_expected.to match %r{HTTP/1.1 308 Permanent Redirect} }
      its(:stdout) { is_expected.to match %r{Location: /fr/} }
      its(:stdout) { is_expected.to match %r{x-comptoir-webapp: 1} }
    end

    describe command('curl -i http://127.0.0.1/fr/') do
      its(:stdout) { is_expected.to match %r{HTTP/1.1 200 OK} }
      its(:stdout) { is_expected.to match %r{x-comptoir-webapp: 1} }
    end

    describe command('curl --head http://127.0.0.1/health-check') do
      its(:stdout) { is_expected.to match %r{HTTP/1.1 200 OK} }
      its(:stdout) { is_expected.to match %r{x-comptoir-database-status: DB_CONNECTION_SUCCESSFUL} }
      its(:stdout) { is_expected.to match %r{x-comptoir-version: #{version_upgrade}} }
    end

    describe file("#{sys_rootpath}/#{download_checksum_initial}") do
      it { is_expected.not_to exist }
    end
  end
end
