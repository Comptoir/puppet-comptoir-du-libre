require 'beaker-rspec'
require 'beaker-puppet'
require 'beaker/puppet_install_helper'
require 'beaker/module_install_helper'

run_puppet_install_helper
install_module_on(hosts)
install_module_dependencies_on(hosts)

php_version = '8.1'
sys_rootpath = '/var/www/comptoir'
vhost_dir = 'comptoir'
download_checksum_initial = '2d9099a51b3cd43633e7784e50f734aaf76206660c8c03f5c5f9549899ce22c9' # 3.0.0.alpha.7
download_checksum_upgrade = 'aafabaaf7ca309569d087116d6067e9ccbf8ad3a699bd28a2c5dd451430cfdf0' # 3.0.0.alpha.8
archive_rootdir = 'comptoir'
public_dir = 'public'

RSpec.configure do |c|
  # Configure all nodes in nodeset
  c.before :suite do
    # Additional module and configuration for being able to setup web server requirements
    install_module_from_forge('puppetlabs-apache', '>= 10.1.1 < 13.0.0')

    pp_helper = %(
      exec { 'mkdir initial #{archive_rootdir}/#{public_dir}':
        command => '/usr/bin/mkdir -p #{sys_rootpath}/#{download_checksum_initial}/#{archive_rootdir}/#{public_dir}',
      }
      -> exec { 'mkdir upgrade #{archive_rootdir}/#{public_dir}':
        command => '/usr/bin/mkdir -p #{sys_rootpath}/#{download_checksum_upgrade}/#{archive_rootdir}/#{public_dir}',
      }
      -> exec { 'chown www-data #{archive_rootdir}/#{public_dir}':
        command => '/usr/bin/chown -R www-data:www-data #{sys_rootpath}',
      }

      file { '#{sys_rootpath}/#{vhost_dir}/':
        ensure => 'link',
        target => '#{sys_rootpath}',
      }
      -> class { 'apache':
        default_vhost => false,
        default_mods  => ['php', 'rewrite'],
        mpm_module    => 'prefork',
        require       => Exec['chown www-data #{archive_rootdir}/#{public_dir}'],
      }
      apache::vhost { 'comptoir':
        port          => 80,
        docroot       => '#{sys_rootpath}/#{vhost_dir}/#{public_dir}',
        docroot_owner => 'www-data',
        docroot_group => 'www-data',
        override      => ['All'],
      }

      $php_libs = [
        'php#{php_version}-curl',
        'php#{php_version}-intl',
        'php#{php_version}-pgsql',
        'php#{php_version}-mbstring',
        'php#{php_version}-xml',
      ]
      package { $php_libs:
        ensure  => present,
      }
    )
    apply_manifest(pp_helper, catch_failures: true)
  end
end
