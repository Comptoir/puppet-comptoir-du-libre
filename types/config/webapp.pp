# Describes configuration parameters
type Comptoir::Config::Webapp = Hash[
  String[1],
  Variant[
    String[1],
    Integer,
    Boolean,
  ],
]
