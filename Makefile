
RUBY_VERSION='3.2.0'
OS_VERSION_1=ubuntu2204-64

install: clean bundle_install ## Clean + Install dependencies
.PHONY: bundle_install

clean: ## Delete all files installed by bundle install
	rm -f Gemfile.lock || true
	rm -Rf vendor/
	rm -Rf .bundle/
.PHONY: clean

bundle_install: ## Install dependencies
	bundle config set --local path 'vendor/bundle'
	bundle config set --local without 'system_tests'
	bundle config set --local jobs `nproc`
	bundle install
	bundle clean
.PHONY: bundle_install


autocorrect_rubocop: bundle_install ## Autocorrect RuboCop offenses (only when it's safe)
	bundle exec rake rubocop:autocorrect
.PHONY: autocorrect_rubocop

rubocop: bundle_install ## Run RuboCop: a Ruby static code analyzer (a.k.a. linter) and code formatter
	bundle exec rake rubocop
.PHONY: rubocop

linter: bundle_install ## Run all linter: RuboCop and syntax (manifests, templates, hiera)
	bundle exec rake syntax lint metadata_lint check:symlinks check:git_ignore check:dot_underscore check:test_file rubocop
.PHONY: linter


unit_tests: bundle_install ## Run unit tests
	bundle exec rake parallel_spec
.PHONY: linter

acceptance_tests: bundle_install ## Run acceptance tests
	RBENV_VERSION=${RUBY_VERSION}     \
	PUPPET_INSTALL_TYPE=agent         \
	BEAKER_IS_PE='no'                 \
	BEAKER_PUPPET_COLLECTION=puppet7  \
	BEAKER_debug='true'               \
	BEAKER_HYPERVISOR=docker          \
	BEAKER_setfile="${OS_VERSION_1}{hostname=comptoir.example.com}"   \
	bundle exec rake beaker
.PHONY: acceptance_tests

doc_update_reference_file: bundle_install ## Update REFERENCE.md file
	bundle exec puppet strings generate --format markdown --out REFERENCE.md
.PHONY: doc_update_reference_file


# Default goal and help
.DEFAULT_GOAL := help
help: ## Show this help
	@echo "Makefile - COMPTOIR Puppet module"
	@echo ""
	@echo "Usage: make [target]"
	@echo "---------------------------"
	@echo ""
	@echo "Targets:"
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
#	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | sed -e 's/^Makefile:\(.*\)/\1/' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
.PHONY: help
#---------------------------------------------#
